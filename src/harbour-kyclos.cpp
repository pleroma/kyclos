#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <QCoreApplication>
#include <QGuiApplication>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickView>
#include <QScopedPointer>
#include <QtNetwork>
#include <QtQml>
#include <sailfishapp.h>
//#include <QtSystemInfo/QDeviceInfo>
#include "dbus.h"
#include "filedownloader.h"
#include "imageuploader.h"
#include "notifications.h"

int
main(int argc, char *argv[])
{
	QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
	QScopedPointer<QQuickView> view(SailfishApp::createView());
	//QQmlContext *context = view.data()->rootContext();
	QQmlEngine *engine = view->engine();

	FileDownloader *fd = new FileDownloader(engine);
	view->rootContext()->setContextProperty("FileDownloader", fd);
	qmlRegisterType<ImageUploader>("harbour.kyclos.Uploader", 1, 0, "ImageUploader");

	Notifications *no = new Notifications();
	view->rootContext()->setContextProperty("Notifications", no);
	QObject::connect(engine, SIGNAL(quit()), app.data(), SLOT(quit()));

	Dbus *dbus = new Dbus();
	view->rootContext()->setContextProperty("Dbus", dbus);

	view->setSource(SailfishApp::pathTo("qml/harbour-kyclos.qml"));
	view->show();
	return app->exec();
}
