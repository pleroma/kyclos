<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation>favoriserad</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>följer dig</translation>
    </message>
    <message>
        <source>boosted</source>
        <translation>puffat</translation>
    </message>
    <message>
        <source>said</source>
        <translation>sade</translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation>Öppna i webbläsare</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation>Konversation</translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation>Innehållsvarning!</translation>
    </message>
    <message>
        <source>public</source>
        <translation>publik</translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation>olistad</translation>
    </message>
    <message>
        <source>followers only</source>
        <translation>endast följare</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>direkt</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Radera</translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation>Emojis</translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation>Tryck för att infoga</translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation>Problem att ladda</translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation>Filen %1 går inte att hitta</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation>Hem</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifieringar</translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation>Ny toot</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation>@user eller #term</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Lokalt</translation>
    </message>
    <message>
        <source>Federated</source>
        <translation>Förenade</translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation>puffade</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>favoriserade</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>följer dig</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation>Ladda mer</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>Laddar</translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation>Vänligen vänta...</translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation>Sluta följa</translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation>Följarförfrågan har skickats!</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>Följer</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Tysta</translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation>Avtysta</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>Avblockera</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Blockera</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Statusar</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Favoriter</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Följ</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Översikt</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>Följare</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Inställningar</translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation>puffade</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>favoriserad</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>följer dig</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation>Avpuffa</translation>
    </message>
    <message>
        <source>Boost</source>
        <translation>Puffa</translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation>Avfavorisera</translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation>Favorisera</translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
