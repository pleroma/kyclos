<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation>favourited</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>followed you</translation>
    </message>
    <message>
        <source>boosted</source>
        <translation>boosted</translation>
    </message>
    <message>
        <source>said</source>
        <translation>said</translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation>Open in Browser</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished">Reload</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation>Conversation</translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation>Content warning!</translation>
    </message>
    <message>
        <source>public</source>
        <translation>public</translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation>unlisted</translation>
    </message>
    <message>
        <source>followers only</source>
        <translation>followers only</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>direct</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation>Emojis</translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation>Tap to insert</translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Instance</source>
        <translation>Instance</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Reload</translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation></translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation></translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation></translation>
    </message>
    <message>
        <source>Search</source>
        <translation></translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation></translation>
    </message>
    <message>
        <source>Local</source>
        <translation></translation>
    </message>
    <message>
        <source>Federated</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation>boosted</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>favourited</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>followed you</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation>Load more</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation></translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation>Unfollow</translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation>Follow request sent!</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>Following</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Mute</translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation>Unmute</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>Unblock</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Block</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Statuses</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Favourites</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Follow</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Summary</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>Followers</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation>Remove Account</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Add Account</translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation>Deauthorize this app and remove your account</translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation>Authorize this app to use your Mastodon account in your behalf</translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation>Load images in toots</translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation>Disable this option if you want to preserve your data connection</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Credits</translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation>boosted</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>favourited</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>followed you</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation>Unboost</translation>
    </message>
    <message>
        <source>Boost</source>
        <translation>Boost</translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation>Unfavorite</translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation>Favorite</translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
