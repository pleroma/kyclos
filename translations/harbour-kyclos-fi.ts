<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>followed you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>boosted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>said</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>public</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>followers only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>direct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Federated</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>favourited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>followed you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Following</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Follow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Followers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>favourited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>followed you</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
