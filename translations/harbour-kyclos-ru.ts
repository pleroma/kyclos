<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation>избранное</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>следую за тобой</translation>
    </message>
    <message>
        <source>boosted</source>
        <translation>росту</translation>
    </message>
    <message>
        <source>said</source>
        <translation>сказал</translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation>Открыть в браузере</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation>Беседа</translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation>Предупреждение о содержании!</translation>
    </message>
    <message>
        <source>public</source>
        <translation>публика</translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation>не указан</translation>
    </message>
    <message>
        <source>followers only</source>
        <translation>только последователи</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>непосредственный</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation>Cмайликов</translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation>Нажмите, чтобы вставить</translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation>Ошибка при загрузке</translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation>Файл %1 не существует</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation>Главная</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Уведомления</translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation>Новый</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation>@пользователь или #срок</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Локальный</translation>
    </message>
    <message>
        <source>Federated</source>
        <translation>Федеративные</translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation>росту</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>любимый</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>следую за тобой</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation>Загрузи больше</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>Загрузка</translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation>Пожалуйста, подождите...</translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation>Отписаться</translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation>Следуйте запрошенному запросу!</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>Следующий</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Безгласный</translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation>Нет безгласный</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Блокировать</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Статусы</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Избранные</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Следить</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Резюме</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>Читают</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Настройки</translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation>росту</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>имеет любимый</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>следую за тобой</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation>Нет росту</translation>
    </message>
    <message>
        <source>Boost</source>
        <translation>Росту</translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation>Избранные нет</translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation>Избранные</translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
