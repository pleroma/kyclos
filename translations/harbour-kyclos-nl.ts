<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation>favoriet gemaakt</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>volgde jou</translation>
    </message>
    <message>
        <source>boosted</source>
        <translation>boostte</translation>
    </message>
    <message>
        <source>said</source>
        <translation>zei</translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation>Openen in browser</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation>Gesprek</translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation>Gevoelige inhoud!</translation>
    </message>
    <message>
        <source>public</source>
        <translation>openbaar</translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation>niet op lijst</translation>
    </message>
    <message>
        <source>followers only</source>
        <translation>alleen volgers</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>direct</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation>Emojis</translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation>Tikken om in te voegen</translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation>Fout bij laden</translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation>Bestand %1 bestaat niet</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation>Thuis</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Meldingen</translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation>Nieuwe Toot</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation>@user of #term</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Lokaal</translation>
    </message>
    <message>
        <source>Federated</source>
        <translation>Gefedereerd</translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation>heeft geboost</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>favoriet gemaakt</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>volgde jou</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation>Meer laden</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>Laden</translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation>even geduld…</translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation>Ontvolgen</translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation>Volgverzoek verstuurd!</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>Volgend</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Dempen</translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation>Ontdempen</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>Deblokkeren</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Blokkeren</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Statussen</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Favorieten</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Volgen</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Samenvatting</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>Volgers</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Instellingen</translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation>boostte</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>maakte favoriet</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>volgde jou</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation>Unboost</translation>
    </message>
    <message>
        <source>Boost</source>
        <translation>Boost</translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation>Demarkeren als favoriet</translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation>Markeren als favoriet</translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
