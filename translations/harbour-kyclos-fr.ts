<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation>a ajouté à ses favoris</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>vous suit</translation>
    </message>
    <message>
        <source>boosted</source>
        <translation>a partagé</translation>
    </message>
    <message>
        <source>said</source>
        <translation>a dit</translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished">Recharger</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation>Conversation</translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation>Contenu sensible !</translation>
    </message>
    <message>
        <source>public</source>
        <translation>public</translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation>non listé</translation>
    </message>
    <message>
        <source>followers only</source>
        <translation>abonnés seulement</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>direct</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation>Emojis</translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation>Appuyez pour insérer</translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation>Erreur de chargement</translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation>Le fichier %1 n&apos;existe pas</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>Authentification</translation>
    </message>
    <message>
        <source>Instance</source>
        <translation>Instance</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Recharger</translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation>Nouveau pouet</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation>@personne ou #terme</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <source>Federated</source>
        <translation>Fédéré</translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation>a partagé</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>a ajouté à ses favoris</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>vous a suivi</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation>Charger plus</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation>patientez...</translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation>Ne plus suivre</translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation>Demande de suivi envoyée !</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>Abonnements</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Masquer</translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation>Démasquer</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>Débloquer</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Bloquer</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Suivre</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>Abonnés</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation>Enlever le compte</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Ajouter un compte</translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation>Désautoriser cette application et enlever votre compte</translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation>Autoriser cette application à utiliser votre compte</translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation>Charger les images dans les pouets</translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation>Désactiver cette option pour économiser les données mobiles</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Crédits</translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation>boosté</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>a ajouté à ses favoris</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>vous suit</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation>ne plus partager</translation>
    </message>
    <message>
        <source>Boost</source>
        <translation>partager</translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation>supprimer de ses favoris</translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation>ajouter aux favoris</translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
