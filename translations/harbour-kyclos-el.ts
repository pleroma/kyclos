<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation>στους σελιδοδείκτες</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>σας ακολουθούν</translation>
    </message>
    <message>
        <source>boosted</source>
        <translation>προωθημένο</translation>
    </message>
    <message>
        <source>said</source>
        <translation>είπε</translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation>Άνοιγμα στον φυλλομετρητή</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation>Συνομιλία</translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation>Προειδοποίηση περιεχομένου!</translation>
    </message>
    <message>
        <source>public</source>
        <translation>δημόσιο</translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation>μη καταχωρημένο</translation>
    </message>
    <message>
        <source>followers only</source>
        <translation>μόνο αυτοί που σας ακολουθούν</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>απευθείας</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Διαγραφή</translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation>Emoji</translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation>Κτυπήστε για εισαγωγή</translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation>Σφάλμα φόρτωσης</translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation>Το αρχείο %1 δεν υπάρχει</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation>Οικοσελίδα</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Ειδοποιήσεις</translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation>Νέος</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Αναζήτηση</translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation>@χρήστη ή #όρος</translation>
    </message>
    <message>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Federated</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation>προωθημένο</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>στους σελιδοδείκτες</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>σας ακολουθούν</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation>Φόρτωση περισσοτέρων</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Ρυθμίσεις</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>Φόρτωση</translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation>Αναίρεση παρακολούθησης</translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation>Η αίτηση παρακολούθησης εστάλη!</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>Σε παρακολούθηση</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Σίγαση</translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation>Αναίρεση σίγασης</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>Αναίρεση φραγής</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Φραγή</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Κατάσταση</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Σελιδοδείκτες</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Παρακολούθηση</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Σύνοψη</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>Σας ακολουθούν</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Ρυθμίσεις</translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation>προωθημένο</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>στους σελιδοδείκτες</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>σας ακολουθούν</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation>Αναίρεση προώθησης</translation>
    </message>
    <message>
        <source>Boost</source>
        <translation>Προώθηση</translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation>Αφαίρεση από τους σελιδοδείκτες</translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation>Σελιδοδείκτης</translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
