<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation>收藏</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>关注你的</translation>
    </message>
    <message>
        <source>boosted</source>
        <translation>推起的</translation>
    </message>
    <message>
        <source>said</source>
        <translation>说的</translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation>在浏览器打开</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished">重新加载</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation>对话</translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation>内容警告!</translation>
    </message>
    <message>
        <source>public</source>
        <translation>公共区域</translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation>未列的</translation>
    </message>
    <message>
        <source>followers only</source>
        <translation>仅关注者</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>直接</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation>表情</translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation>点击插入</translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation>加载错误</translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation>文件 %1 不存在</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>Instance</source>
        <translation>实例</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>重新加载</translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation>主页</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>通知</translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation>新嘟嘟</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation>@用户或#项目</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>本地</translation>
    </message>
    <message>
        <source>Federated</source>
        <translation>联合的</translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation>推起的</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>收藏</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>关注你的</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation>加载更多</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>加载中</translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation>稍等片刻......</translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation>未关注</translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation>已寄出关注请求！</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>关注中</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>静音</translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation>未静音</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>解除封锁</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>封锁</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>状态</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>收藏</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>关注</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>摘要</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>关注者</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation>移除账户</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>添加账户</translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation>取消对此软件的授权并移除你的账户</translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation>授权此软件使用你的 Mastodon 账户</translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation>在嘟嘟加载图片</translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation>如果你想保护你的数据连接 请禁用此操作</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>信誉</translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation>推起的</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>收藏</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>关注你的</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation>取消推起</translation>
    </message>
    <message>
        <source>Boost</source>
        <translation>推起</translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation>取消收藏</translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation>收藏</translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
