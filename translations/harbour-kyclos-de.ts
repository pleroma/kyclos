<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>API</name>
    <message>
        <source>favourited</source>
        <translation>favorisiert</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>sind dir gefolgt</translation>
    </message>
    <message>
        <source>boosted</source>
        <translation>verstärkt</translation>
    </message>
    <message>
        <source>said</source>
        <translation>hat gesagt</translation>
    </message>
    <message>
        <source>reacted with: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Browser</name>
    <message>
        <source>Open in Browser</source>
        <translation>Öffne in Browser</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation type="unfinished">Neu laden</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <source>Conversation</source>
        <translation>Konversation</translation>
    </message>
    <message>
        <source>Content warning!</source>
        <translation>Inhaltswarnung!</translation>
    </message>
    <message>
        <source>public</source>
        <translation>öffentlich</translation>
    </message>
    <message>
        <source>unlisted</source>
        <translation>nicht aufgeführt</translation>
    </message>
    <message>
        <source>followers only</source>
        <translation>nur Follower</translation>
    </message>
    <message>
        <source>direct</source>
        <translation>direkt</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <source>Emojis</source>
        <translation>Emojis</translation>
    </message>
    <message>
        <source>Tap to insert</source>
        <translation>Tippen um einzufügen</translation>
    </message>
</context>
<context>
    <name>ImageFullScreen</name>
    <message>
        <source>Error loading</source>
        <translation>Fehler beim Laden</translation>
    </message>
</context>
<context>
    <name>ImageUploader</name>
    <message>
        <source>The file %1 does not exists</source>
        <translation>Die Datei %1 existiert nicht</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Instance</source>
        <translation>Instanz</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <source>Enter an instance URL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Benachrichtigungen</translation>
    </message>
    <message>
        <source>New Toot</source>
        <translation>Neuer Toot</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <source>@user or #term</source>
        <translation>@User oder #Ausdruck</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Lokal</translation>
    </message>
    <message>
        <source>Federated</source>
        <translation>Föderiert</translation>
    </message>
</context>
<context>
    <name>MiniStatus</name>
    <message>
        <source>boosted</source>
        <translation>verstärkt</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>favorisiert</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>sind dir gefolgt</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyList</name>
    <message>
        <source>Load more</source>
        <translation>Lade mehr</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Loading</source>
        <translation>Lädt...</translation>
    </message>
    <message>
        <source>please wait...</source>
        <translation>bitte warten...</translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <source>Unfollow</source>
        <translation>Nicht mehr folgen</translation>
    </message>
    <message>
        <source>Follow request sent!</source>
        <translation>Folge-Anfrage gesendet!</translation>
    </message>
    <message>
        <source>Following</source>
        <translation>Folgend</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Stumm</translation>
    </message>
    <message>
        <source>Unmute</source>
        <translation>Nicht stumm</translation>
    </message>
    <message>
        <source>Unblock</source>
        <translation>Nicht blockieren</translation>
    </message>
    <message>
        <source>Block</source>
        <translation>Blockieren</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Favourites</source>
        <translation>Favoriten</translation>
    </message>
    <message>
        <source>Follow</source>
        <translation>Folge</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <source>Followers</source>
        <translation>Anhänger</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Remove Account</source>
        <translation>Konto entfernen</translation>
    </message>
    <message>
        <source>Add Account</source>
        <translation>Konto hinzufügen</translation>
    </message>
    <message>
        <source>Deauthorize this app and remove your account</source>
        <translation>Konto entfernen und für diese Anwendung deaktivieren</translation>
    </message>
    <message>
        <source>Authorize this app to use your Mastodon account in your behalf</source>
        <translation>Zugriff durch diese Anwendung auf eigenes Mastodon-Konto erlauben</translation>
    </message>
    <message>
        <source>Load images in toots</source>
        <translation>Bilder in Toots laden</translation>
    </message>
    <message>
        <source>Disable this option if you want to preserve your data connection</source>
        <translation>Diese Option deaktivieren um Datenvolumen zu sparen</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Dank an</translation>
    </message>
    <message>
        <source>Pleroma Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forking Tooter to Kyclos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tooter developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>visual identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Occitan &amp; French translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Español translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Added README file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toot</name>
    <message>
        <source>boosted</source>
        <translation>verstärkt</translation>
    </message>
    <message>
        <source>favourited</source>
        <translation>favorisiert</translation>
    </message>
    <message>
        <source>followed you</source>
        <translation>folgt dir</translation>
    </message>
    <message>
        <source>reacted with</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VisualContainer</name>
    <message>
        <source>Unboost</source>
        <translation>Schwäche</translation>
    </message>
    <message>
        <source>Boost</source>
        <translation>Verstärke</translation>
    </message>
    <message>
        <source>Unfavorite</source>
        <translation>Aus Favoriten entfernen</translation>
    </message>
    <message>
        <source>Favorite</source>
        <translation>Zu Favoriten</translation>
    </message>
    <message>
        <source>Unbookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
