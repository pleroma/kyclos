# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-kyclos

CONFIG += sailfishapp

QT += network dbus sql
CONFIG += link_pkgconfig
PKGCONFIG += sailfishapp
PKGCONFIG += \
    nemonotifications-qt5

DEFINES += "APPVERSION=\\\"$${SPECVERSION}\\\""
DEFINES += "APPNAME=\\\"$${TARGET}\\\""

!exists( src/dbusAdaptor.h ) {
    system(qdbusxml2cpp config/social.pleroma.harbour.kyclos.xml -i dbus.h -a src/dbusAdaptor)
}

config.path = /usr/share/$${TARGET}/config/
config.files = config/icon-lock-harbour-kyclos.png

notification_categories.path = /usr/share/lipstick/notificationcategories
notification_categories.files = config/x-harbour.kyclos.activity.*

dbus_services.path = /usr/share/dbus-1/services/
dbus_services.files = config/social.pleroma.harbour.kyclos.service

interfaces.path = /usr/share/dbus-1/interfaces/
interfaces.files = config/social.pleroma.harbour.kyclos.xml

SOURCES += src/*.cpp
HEADERS += src/*.h

lupdate_only{
SOURCES += \
    qml/*.qml \
    qml/cover/*.qml \
    qml/pages/*.qml \
    qml/pages/components/*.qml \
    qml/lib/*.js
}

DISTFILES += \
    qml/*.qml \
    qml/cover/*.qml \
    qml/pages/*.qml \
    qml/pages/components/*.qml \
    qml/lib/*.js

DISTFILES += \
    qml/images/*.svg \
    config/icon-lock-harbour-kyclos.png \
    config/x-harbour.kyclos.activity.conf \
    rpm/harbour-kyclos.changes \
    rpm/harbour-kyclos.changes.run.in \
    rpm/harbour-kyclos.spec \
    rpm/harbour-kyclos.yaml \
    harbour-kyclos.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-kyclos.ts
TRANSLATIONS += translations/harbour-kyclos-de.ts
TRANSLATIONS += translations/harbour-kyclos-el.ts
TRANSLATIONS += translations/harbour-kyclos-es.ts
TRANSLATIONS += translations/harbour-kyclos-fi.ts
TRANSLATIONS += translations/harbour-kyclos-fr.ts
TRANSLATIONS += translations/harbour-kyclos-nl.ts
TRANSLATIONS += translations/harbour-kyclos-nl_BE.ts
TRANSLATIONS += translations/harbour-kyclos-oc.ts
TRANSLATIONS += translations/harbour-kyclos-pl.ts
TRANSLATIONS += translations/harbour-kyclos-ru.ts
TRANSLATIONS += translations/harbour-kyclos-sr.ts
TRANSLATIONS += translations/harbour-kyclos-sv.ts
TRANSLATIONS += translations/harbour-kyclos-zh_CN.ts
