/*
	Copyright (C) 2013 Jolla Ltd.
	Contact: Thomas Perl <thomas.perl@jollamobile.com>
	All rights reserved.

	You may use this file under the terms of BSD license as follows:

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
		* Redistributions of source code must retain the above copyright
			notice, this list of conditions and the following disclaimer.
		* Redistributions in binary form must reproduce the above copyright
			notice, this list of conditions and the following disclaimer in the
			documentation and/or other materials provided with the distribution.
		* Neither the name of the Jolla Ltd nor the
			names of its contributors may be used to endorse or promote products
			derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import QtQuick 2.0
import QtWebKit 3.0
import Sailfish.Silica 1.0
import "../lib/API.js" as Logic

Page {
	id: loginPage

	// The effective value will be restricted by ApplicationWindow.allowedOrientations
	allowedOrientations: Orientation.All

	function instance_login() {
		Logic.api = new Logic.MastodonAPI(
			{"instance": instance.text, "api_user_token": ""}
		)
		Logic.api.registerApplication(
			"Kyclos", 'http://localhost/harbour-kyclos',
			// redirect uri, we will need this later on
			["read", "write", "follow"], //scopes
			"https://git.pleroma.social/pleroma/harbour-kyclos",
			//website on the login screen
			function (data) {
				var conf = JSON.parse(data);
				conf.instance = instance.text;
				conf.login = false;

				Logic.conf = conf;

				var url = Logic.api.generateAuthLink(
					Logic.conf["client_id"],
					Logic.conf["redirect_uri"],
					"code", // oauth method
					["read", "write", "follow"] //scopes
				)

				webView.url = url;
				webView.visible = true;
			}
		)
	}

	SilicaFlickable {
		anchors.fill: parent
		contentHeight: column.height + Theme.paddingLarge

		VerticalScrollDecorator {}

		Column {
			id: column
			width: parent.width
			PageHeader { title: qsTr("Login") }
			SectionHeader { text: qsTr("Instance") }

			TextField {
				id: instance
				focus: true
				label: qsTr("Enter an instance URL")
				text: "https://"
				placeholderText: "https://social.example.org"
				width: parent.width
				validator: RegExpValidator {
					regExp: /^(http|https):\/\/[^ "]+$/
				}
				EnterKey.enabled: instance.acceptableInput
				EnterKey.iconSource: "image://theme/icon-m-enter-next"
				EnterKey.onClicked: {
					instance_login()
				}
			}

			IconButton {
				id: login
				anchors {
					right: parent.right
				}
				onClicked: {
					instance_login()
				}
				icon.source: "image://theme/icon-m-enter-next" + "?"
										 + (pressed ? Theme.highlightColor : Theme.primaryColor)
			}
		}
	}

	SilicaWebView {
		id: webView
		visible: false
		anchors {
			top: parent.top
			left: parent.left
			right: parent.right
			bottom: parent.bottom
		}

		opacity: 1
		onLoadingChanged: {
			if (
				(url + "").substr(0, 37) === 'http://localhost/harbour-kyclos?code=' ||
				(url + "").substr(0, 38) === 'https://localhost/harbour-kyclos?code='
			) {
				visible = false;

				/* found on https://html-online.com/articles/get-url-parameters-javascript/ */
				var vars = {};
				(url + "").replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {vars[key] = value});

				var authCode = vars["code"];

				Logic.api.getAccessTokenFromAuthCode(
					Logic.conf["client_id"],
					Logic.conf["client_secret"],
					Logic.conf["redirect_uri"],
					authCode,
					function (data) {
						// AAAND DATA CONTAINS OUR TOKEN!
						data = JSON.parse(data);
						Logic.conf["api_user_token"] = data.access_token;
						Logic.conf["login"] = true;
						Logic.api.setConfig("api_user_token", Logic.conf["api_user_token"]);
						pageStack.replace(Qt.resolvedUrl("MainPage.qml"), { });
					}
				)
			}
		}

		PullDownMenu {
			MenuItem {
				text: qsTr("Reload")
				onClicked: webView.reload()
			}
		}
	}
}
