# Kyclos

<img width="64px" src="./icons/256x256/harbour-kyclos.png" />

Kyclos is a Pleroma client for Sailfish OS (Silica Qt).

## Installing

You can grab it from OpenRepos: <https://openrepos.net/content/lanodan/kyclos>

Installing a openrepos client such as Storeman is recommended to manage user-repositories.

## Building

Provided you already have the dependencies:
```
qmake
make
```

Reformatting non-QML files is done with clang-format: ``clang-format -style=file -assume-filename=.clang-format -i src/*.cpp src/*.h qml/lib/*.js``

If you need a platform to build it, you can use your probably already existent SailfishOS phone (in developer mode), the SailfishOS SDK. Sadly this won't build without some of the proprietary SailfishOS components (otherwise this would be a regular Qt QML application).

## Launching
### From terminal/SSH
```
invoker --type=silica-qt5 path/to/harbour-tooter
```
Invoker allows to avoid the issue of not getting a virtual keyboard amongs maybe some others.

If you want to run it without installing you need to do the following:
```
cd ..
ln -s ./ share
```

## Screenshots

<img width="200px" src="./screenshots/Screenshot_20200123_003.png" />
<img width="200px" src="./screenshots/Screenshot_20200123_006.png" />
<img width="200px" src="./screenshots/Screenshot_20200123_007.png" />
